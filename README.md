# Project: armando-app

by: Iceyisaak

### This project features:

- Corporate Website
- Fixed Collapsible Navbar (top)
- Card Elements
- Css Grid Gallery

### Technologies used
- Pure HTML/CSS (SASS)
- FontAwesome
- GoogleFonts

## Installation
1. `npm install` to create node_modules
2. `npm run start` to run the project on live-server
3. ENJOY!

